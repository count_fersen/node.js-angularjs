var express = require('express');
var bodyParser = require('body-parser');
var routes = require('./rhs/routes/CustomerRoutes');

var app = express();
var port = process.env.PORT || 1337;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app); //register the route

app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'});
});
app.listen(port);

console.log('Server running at http://127.0.0.1:1337/');