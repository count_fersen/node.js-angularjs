'use strict';

const { Pool } = require('pg');
const connectionString = 'postgresql://postgres:elephant@localhost:5432/postgres';

const pool = new Pool({
  connectionString: connectionString,
});

exports.getAllCustomer = function(req, res) {
	pool.connect((err, client, done) => {
		if (err) {
			done();
			throw err;
		}
		client.query('SELECT * FROM customer', (err, result) => {
		    done();
		    if (err) {
		    	console.log(err.stack);
		    } else {
		    	res.json(result.rows);
		    }
		});
	});
};

exports.saveCustomer = function(req, res) {
	const { firstName, lastName } = req.body;
	pool.connect((err, client, done) => {
		if (err) {
			done();
			throw err;
		}
		client.query('BEGIN', (err, result) => {
			if (err) {
				done();
		    	console.log(err.stack);
		    } else {
		    	client.query('INSERT INTO customer(first_name, last_name) VALUES ($1, $2)', [firstName, lastName], (err, result) => {
				    if (err) {
				    	done();
				    	console.log(err.stack);
				    } else {
				    	client.query('COMMIT', (err, result) => {
						    done();
						    if (err) {
						    	console.log(err.stack);
						    } else {
						    	res.json(result);
						    }
						});
				    }
				});
		    }
		});
	});
};

exports.getCustomer = function(req, res) {
	pool.connect((err, client, done) => {
		if (err) {
			done();
			throw err;
		}
		client.query('SELECT * FROM customer WHERE id = $1', [req.params.id], (err, result) => {
		    done();
		    if (err) {
		    	console.log(err.stack);
		    } else {
		    	res.json(result.rows);
		    }
		});
	});
};

exports.updateCustomer = function(req, res) {
	const id = req.params.id;
	const { firstName, lastName } = req.body;
	pool.connect((err, client, done) => {
		if (err) {
			done();
			throw err;
		}
		client.query('BEGIN', (err, result) => {
			if (err) {
				done();
		    	console.log(err.stack);
		    } else {
		    	client.query('UPDATE customer SET first_name = $1, last_name = $2 WHERE id = $3', [firstName, lastName, id], (err, result) => {
				    if (err) {
				    	done();
				    	console.log(err.stack);
				    } else {
				    	client.query('COMMIT', (err, result) => {
						    done();
						    if (err) {
						    	console.log(err.stack);
						    } else {
						    	res.json(result);
						    }
						});
				    }
				});
		    }
		});
	});
};

exports.deleteCustomer = function(req, res) {
	const id = req.params.id;
	pool.connect((err, client, done) => {
		if (err) {
			done();
			throw err;
		}
		client.query('BEGIN', (err, result) => {
			if (err) {
				done();
		    	console.log(err.stack);
		    } else {
		    	client.query('DELETE FROM customer WHERE id = $1', [id], (err, result) => {
				    if (err) {
				    	done();
				    	console.log(err.stack);
				    } else {
				    	client.query('COMMIT', (err, result) => {
						    done();
						    if (err) {
						    	console.log(err.stack);
						    } else {
						    	res.json(result);
						    }
						});
				    }
				});
		    }
		});
	});
};