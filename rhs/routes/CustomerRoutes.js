'use strict';
module.exports = function(app) {
  var controller = require('../controllers/CustomerController');

  // Customer Routes
  app.route('/customer')
    .get(controller.getAllCustomer)
    .post(controller.saveCustomer);


  app.route('/customer/:id')
    .get(controller.getCustomer)
    .put(controller.updateCustomer)
    .delete(controller.deleteCustomer);
};